import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Dashboard from './components/pages/Dashboard';
import DetailPage from './components/pages/DetailPage';
import './App.css';

const App = ({ location }) => (

  <div className="app">
    <header>
    </header>
    <BrowserRouter>
      <Switch>
      <Route location={location} path='/detail/:account_id' exact component={DetailPage} />
        <Route location={location} path='/' exact component={Dashboard} />
      </Switch>
    </BrowserRouter>
  </div>

);

App.prototype = {
  location: PropTypes.shape({
    pathName: PropTypes.string.isRequired
  }).isRequired
}

export default App;

