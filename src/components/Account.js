import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../styles/Account.scss'

class Account extends Component {
    render() {
        return (
            <div className="post">
                <p className="description">{this.props.account.cuentaId} - {this.props.account.tipoCuenta}</p>
                <p className="account-balance">$ {this.props.account.valorDisponible}</p>
                <p className="text-bot">Saldo disponible</p>
                <Link to={'/detail/' + this.props.account.cuentaId}><p>Ver Detalles</p></Link>
            </div>
        );
    }
}

export default Account;
