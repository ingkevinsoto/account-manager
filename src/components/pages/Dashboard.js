import React, { Component } from 'react';
import '../../styles/Dashboard.scss';
import axios from 'axios';
import Account from '../Account';


class Dashboard extends Component {
  state = {
    accounts: [],
    page: 1,
    hasMore: true
  };

  render() {
    return (
      <div className="dash">
        <div>
          <h2 className="title">Mis Cuentas</h2>
          <hr />
        </div>
        <div className="content">

          {this.state.accounts.sort((a, b) => {
            var tipoB = b.tipoCuenta;
            var tipoA = a.tipoCuenta;
            var bVal = b.valorDisponible;
            var aVal = a.valorDisponible;
        
            if(tipoB == tipoA)
            {
                return (bVal < aVal) ? -1 : (bVal > aVal) ? 1 : 0;
            }
            else
            {
                return (tipoB < tipoA) ? -1 : 1;
            }
          })
          .map(account => {
            return <div
              key={account.id}
              className="card">
              <Account account={account} />
            </div>
          })}

        </div>
      </div>
    );
  }

  componentDidMount() {
    axios
      .get(`http://www.mocky.io/v2/5e24da753400008983012cfa`)
      .then(res => {
        console.log(res.data)
        this.setState({ accounts: res.data });
      })
  }

  fetchAccounts = () => {
    this.setState({ page: this.state.page + 1 });

    axios
      .get(`/?page=${this.state.page}`)
      .then(res => {
        this.setState({ accounts: this.state.accounts.concat(res.data.results) });
      }).catch((error) => {
        this.setState({ hasMore: false })
      }
      )

  };
}

export default Dashboard;
