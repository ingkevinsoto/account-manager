import React, { Component } from 'react';
import axios from 'axios';
import '../../styles/DetailPage.scss';

class DetailPage extends Component {

    state = {
        movements: []
    }

    componentDidMount() {
        let id = this.props.match.params.movements;
        console.log('movements id', id);
        axios.get("http://www.mocky.io/v2/5e24dee73400009a81012d02/" + id)
            .then(res => {
                console.log('res.data', res.data);
                this.setState({ movements: res.data });
                console.log('movements', this.state.movements);
            })
    }

    render() {
        return (
            <div className="content-table">
                <div>
                    <h2 className="title">Mis Movimientos de Cuenta</h2>
                    <hr />
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Cuenta Id</th>
                            <th>Fecha Transaccion</th>
                            <th>Monto Transaccion</th>
                            <th>Descripcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.movements.sort((a, b) => new Date(a.fechaTransaccion) - new Date(b.fechaTransaccion))
                        .map(mov => {
                            return <tr key={mov.id}>
                                <td>{mov.id}</td>
                                <td>{mov.cuentaId}</td>
                                <td>{mov.fechaTransaccion}</td>
                                <td>{mov.montoTransaccion}</td>
                                <td>{mov.descripcion}</td>
                            </tr>
                        })}

                    </tbody>
                </table>
            </div>
        );
    }
}

export default DetailPage;
